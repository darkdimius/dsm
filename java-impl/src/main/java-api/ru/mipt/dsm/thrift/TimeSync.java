/**
 * Autogenerated by Thrift Compiler (0.8.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package ru.mipt.dsm.thrift;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeSync implements org.apache.thrift.TBase<TimeSync, TimeSync._Fields>, java.io.Serializable, Cloneable {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("TimeSync");

  private static final org.apache.thrift.protocol.TField MY_STAMP_FIELD_DESC = new org.apache.thrift.protocol.TField("myStamp", org.apache.thrift.protocol.TType.I64, (short)1);
  private static final org.apache.thrift.protocol.TField REQUEST_STAMP_FIELD_DESC = new org.apache.thrift.protocol.TField("requestStamp", org.apache.thrift.protocol.TType.I64, (short)2);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new TimeSyncStandardSchemeFactory());
    schemes.put(TupleScheme.class, new TimeSyncTupleSchemeFactory());
  }

  private long myStamp; // required
  private long requestStamp; // optional

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    MY_STAMP((short)1, "myStamp"),
    REQUEST_STAMP((short)2, "requestStamp");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // MY_STAMP
          return MY_STAMP;
        case 2: // REQUEST_STAMP
          return REQUEST_STAMP;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __MYSTAMP_ISSET_ID = 0;
  private static final int __REQUESTSTAMP_ISSET_ID = 1;
  private BitSet __isset_bit_vector = new BitSet(2);
  private _Fields optionals[] = {_Fields.REQUEST_STAMP};
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.MY_STAMP, new org.apache.thrift.meta_data.FieldMetaData("myStamp", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    tmpMap.put(_Fields.REQUEST_STAMP, new org.apache.thrift.meta_data.FieldMetaData("requestStamp", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(TimeSync.class, metaDataMap);
  }

  public TimeSync() {
  }

  public TimeSync(
    long myStamp)
  {
    this();
    this.myStamp = myStamp;
    setMyStampIsSet(true);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public TimeSync(TimeSync other) {
    __isset_bit_vector.clear();
    __isset_bit_vector.or(other.__isset_bit_vector);
    this.myStamp = other.myStamp;
    this.requestStamp = other.requestStamp;
  }

  public TimeSync deepCopy() {
    return new TimeSync(this);
  }

  @Override
  public void clear() {
    setMyStampIsSet(false);
    this.myStamp = 0;
    setRequestStampIsSet(false);
    this.requestStamp = 0;
  }

  public long getMyStamp() {
    return this.myStamp;
  }

  public void setMyStamp(long myStamp) {
    this.myStamp = myStamp;
    setMyStampIsSet(true);
  }

  public void unsetMyStamp() {
    __isset_bit_vector.clear(__MYSTAMP_ISSET_ID);
  }

  /** Returns true if field myStamp is set (has been assigned a value) and false otherwise */
  public boolean isSetMyStamp() {
    return __isset_bit_vector.get(__MYSTAMP_ISSET_ID);
  }

  public void setMyStampIsSet(boolean value) {
    __isset_bit_vector.set(__MYSTAMP_ISSET_ID, value);
  }

  public long getRequestStamp() {
    return this.requestStamp;
  }

  public void setRequestStamp(long requestStamp) {
    this.requestStamp = requestStamp;
    setRequestStampIsSet(true);
  }

  public void unsetRequestStamp() {
    __isset_bit_vector.clear(__REQUESTSTAMP_ISSET_ID);
  }

  /** Returns true if field requestStamp is set (has been assigned a value) and false otherwise */
  public boolean isSetRequestStamp() {
    return __isset_bit_vector.get(__REQUESTSTAMP_ISSET_ID);
  }

  public void setRequestStampIsSet(boolean value) {
    __isset_bit_vector.set(__REQUESTSTAMP_ISSET_ID, value);
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case MY_STAMP:
      if (value == null) {
        unsetMyStamp();
      } else {
        setMyStamp((Long)value);
      }
      break;

    case REQUEST_STAMP:
      if (value == null) {
        unsetRequestStamp();
      } else {
        setRequestStamp((Long)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case MY_STAMP:
      return Long.valueOf(getMyStamp());

    case REQUEST_STAMP:
      return Long.valueOf(getRequestStamp());

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case MY_STAMP:
      return isSetMyStamp();
    case REQUEST_STAMP:
      return isSetRequestStamp();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof TimeSync)
      return this.equals((TimeSync)that);
    return false;
  }

  public boolean equals(TimeSync that) {
    if (that == null)
      return false;

    boolean this_present_myStamp = true;
    boolean that_present_myStamp = true;
    if (this_present_myStamp || that_present_myStamp) {
      if (!(this_present_myStamp && that_present_myStamp))
        return false;
      if (this.myStamp != that.myStamp)
        return false;
    }

    boolean this_present_requestStamp = true && this.isSetRequestStamp();
    boolean that_present_requestStamp = true && that.isSetRequestStamp();
    if (this_present_requestStamp || that_present_requestStamp) {
      if (!(this_present_requestStamp && that_present_requestStamp))
        return false;
      if (this.requestStamp != that.requestStamp)
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  public int compareTo(TimeSync other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;
    TimeSync typedOther = (TimeSync)other;

    lastComparison = Boolean.valueOf(isSetMyStamp()).compareTo(typedOther.isSetMyStamp());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetMyStamp()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.myStamp, typedOther.myStamp);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetRequestStamp()).compareTo(typedOther.isSetRequestStamp());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetRequestStamp()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.requestStamp, typedOther.requestStamp);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("TimeSync(");
    boolean first = true;

    sb.append("myStamp:");
    sb.append(this.myStamp);
    first = false;
    if (isSetRequestStamp()) {
      if (!first) sb.append(", ");
      sb.append("requestStamp:");
      sb.append(this.requestStamp);
      first = false;
    }
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    if (!isSetMyStamp()) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'myStamp' is unset! Struct:" + toString());
    }

  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bit_vector = new BitSet(1);
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class TimeSyncStandardSchemeFactory implements SchemeFactory {
    public TimeSyncStandardScheme getScheme() {
      return new TimeSyncStandardScheme();
    }
  }

  private static class TimeSyncStandardScheme extends StandardScheme<TimeSync> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, TimeSync struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // MY_STAMP
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.myStamp = iprot.readI64();
              struct.setMyStampIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // REQUEST_STAMP
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.requestStamp = iprot.readI64();
              struct.setRequestStampIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, TimeSync struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(MY_STAMP_FIELD_DESC);
      oprot.writeI64(struct.myStamp);
      oprot.writeFieldEnd();
      if (struct.isSetRequestStamp()) {
        oprot.writeFieldBegin(REQUEST_STAMP_FIELD_DESC);
        oprot.writeI64(struct.requestStamp);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class TimeSyncTupleSchemeFactory implements SchemeFactory {
    public TimeSyncTupleScheme getScheme() {
      return new TimeSyncTupleScheme();
    }
  }

  private static class TimeSyncTupleScheme extends TupleScheme<TimeSync> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, TimeSync struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      oprot.writeI64(struct.myStamp);
      BitSet optionals = new BitSet();
      if (struct.isSetRequestStamp()) {
        optionals.set(0);
      }
      oprot.writeBitSet(optionals, 1);
      if (struct.isSetRequestStamp()) {
        oprot.writeI64(struct.requestStamp);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, TimeSync struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      struct.myStamp = iprot.readI64();
      struct.setMyStampIsSet(true);
      BitSet incoming = iprot.readBitSet(1);
      if (incoming.get(0)) {
        struct.requestStamp = iprot.readI64();
        struct.setRequestStampIsSet(true);
      }
    }
  }

}

