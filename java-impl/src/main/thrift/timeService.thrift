namespace java ru.mipt.dsm.thrift

struct TimeSync {
    1: required i64 myStamp;
    2: optional i64 requestStamp;
}

service timeService {
    TimeSync getTime (
        1: required TimeSync hostStamp
    );
}
//service wordSearcherService {
//    SearchResults search (
//        1: required string word
//    );
//}
