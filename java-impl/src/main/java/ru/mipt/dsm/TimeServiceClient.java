package ru.mipt.dsm;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.mipt.dsm.services.VersionInfo;
import ru.mipt.dsm.thrift.TimeSync;
import ru.mipt.dsm.thrift.timeService;
import ru.mipt.dsm.tools.types.GitRepoState;

/**
 * User: dark<br>
 * this is client for TimeService. It can grant time synchronization using Kristian algorithm
 */
public class TimeServiceClient {
    private static final Logger logger = LoggerFactory.getLogger(TimeServiceClient.class);

    private final String host;
    private final int port;

    public TimeServiceClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void invoke() throws TException {
        TTransport transport = new TSocket(host, port);
        TProtocol protocol = new TBinaryProtocol(transport);
        timeService.Client client = new timeService.Client(protocol);
        transport.open();
        TimeSync result = client.getTime(new TimeSync(System.currentTimeMillis()));
        long recieveTime = System.currentTimeMillis();
        logger.info("Sever received our request at " + result.getMyStamp());
        logger.info("our request was send at at " + result.getRequestStamp());
        logger.info("we received answer at " + recieveTime);
        logger.info("rtt  is thought to be " + (recieveTime - result.getRequestStamp()) / 2);
        logger.info("thus time should be set to " + (result.getMyStamp() + (recieveTime - result.getRequestStamp()) / 2)
        );
    }

    public static void main(String[] args) {
        logger.info("TimeServiceClient revision " + VersionInfo.gitRepositoryState.commitId);
        if (args.length != 2) {
            logger.info("Usage: TimeServiceClient <serverIp> <serverPort>");

        } else {
            int port = Integer.parseInt(args[1]);
            String host = args[0];
            TimeServiceClient client = new TimeServiceClient(host, port);
            try {
                client.invoke();
            } catch (TException e) {
                logger.error("Failed to invoke client", e);
            }

        }
    }
}
