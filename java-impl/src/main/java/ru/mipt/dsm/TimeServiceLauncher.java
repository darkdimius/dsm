package ru.mipt.dsm;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.mipt.dsm.services.TimeServiceImpl;
import ru.mipt.dsm.services.VersionInfo;
import ru.mipt.dsm.thrift.timeService;

/**
 * User: dark
 * This is helper class that just launches TimeService
 */
public class TimeServiceLauncher {
    private static final Logger logger = LoggerFactory.getLogger(TimeServiceLauncher.class);

    public static void main(String[] args) throws TTransportException {
        logger.info("TimeServiceClient revision " + VersionInfo.gitRepositoryState.commitId);
        boolean portParsed = false;
        int port = 0;
        if (args.length > 0) {
            try {
                port = Integer.parseInt(args[0]);
                portParsed = true;

            } catch (NumberFormatException e) {
                logger.info("failed to parse as port " + args[0]);

            }
        }
        if (args.length != 1 || !portParsed) {
            logger.info("You need to specify service port.");
            logger.info("usage: TimeServiceLauncher <port>");
        } else {
            TServerSocket serverTransport = new TServerSocket(port);
            timeService.Processor processor = new timeService.Processor(new TimeServiceImpl());
            //using binary protocol
            TBinaryProtocol.Factory factory = new TBinaryProtocol.Factory(true, true);
            TServer server = new TThreadPoolServer(new TThreadPoolServer.Args(serverTransport).processor(processor));
            logger.info("starting TimeService at port " + port);
            server.serve();
        }

    }
}
