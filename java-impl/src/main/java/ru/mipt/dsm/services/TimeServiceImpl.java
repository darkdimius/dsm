package ru.mipt.dsm.services;

import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.mipt.dsm.thrift.TimeSync;
import ru.mipt.dsm.thrift.timeService;

/**
 * User: dark<br>
 *     this is time server implementation. It just does all logical job.
 */
public class TimeServiceImpl implements timeService.Iface {
    private static final Logger logger = LoggerFactory.getLogger(TimeServiceImpl.class);

    @Override
    public TimeSync getTime(TimeSync hostStamp) throws TException {
        long answer=System.currentTimeMillis();
        logger.info("request recived with stamp {}, sending answer with {}",hostStamp.getMyStamp(),answer);
        TimeSync result = new TimeSync(answer);
        result.setRequestStamp(hostStamp.getMyStamp());
        return result;
    }
}
