package ru.mipt.dsm.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.mipt.dsm.tools.types.GitRepoState;

import java.io.IOException;
import java.util.Properties;

/**
 * User: dark
 * Date: 10.05.12
 * Time: 20:54
 */
public final class VersionInfo {
    private static final Logger logger = LoggerFactory.getLogger(VersionInfo.class);
    public static final GitRepoState gitRepositoryState;

    static {
        {
            Properties properties = new Properties();
            try {
                properties.load(VersionInfo.class.getClassLoader().getResourceAsStream("git.properties"));
            } catch (IOException e) {
                logger.error("Failed to read version info", e);
                throw new Error("Failed to read version info", e);
            }

            gitRepositoryState = new GitRepoState(properties);
        }
    }


}

